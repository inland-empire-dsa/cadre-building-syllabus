+++
title = "Cadre School Syllabus"
+++

# Cadre School Syllabus

## How to use this Syllabus 

* This syllabus is meant as a resource to help the IEDSA chapter with political education around cadre building.    
* Each topic is broken into session with short-form readings, podcasts, or videos. We encourage you to cover the material in small groups and use the questions as a stepping off point for a dialogue about the content.  
* This syllabus can also serve as a guide for self-directed, self-paced political education.  
* If you're a cadre leader you are encouraged to assign all new members a mentor that will help guide them through the content and create a learning path. Content is meant to not just be read, listened to or watched, but also discussed. Please assign "Red buddies" for the assigned content to help solidify the content and get both members used to understanding and being able to communicate the content.    
* Some sessions are longer form and are optional for members to come back to after covering the quicker material for more in depth readings. These should be made into occassional reading groups.   

