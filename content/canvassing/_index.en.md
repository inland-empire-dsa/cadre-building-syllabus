+++
chapter = true
title = "Canvassing"
weight = 2
+++

### Chapter 2

# Canvassing

Discover how to canvass internally within the organization and externally in your neighborhood.
