+++
disableToc = true
title = "What is a Cadre?"
weight = 5
+++


> Therefore the cadre person is creative, a leader of high standing, a technician with a good political level, who by reasoning dialectically can advance his sector of production, or develop the masses from his position of political leadership. -  Ernesto Che Guevara  

Read Article [The Cadres: Backbone of the Revolution](https://www.marxists.org/archive/guevara/1962/09/misc/x01.htm)  
