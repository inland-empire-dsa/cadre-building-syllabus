+++
disableToc = false
title = "The New Intellectual"
weight = 15
+++

The New Intellectual provides a short assessment of Castro’s Battle of Ideas and the task that lies before new intellectuals in our current political context in order to build a world where our productive capacity enriches all of us. It is essential that we participate in the conversation about what a transformed society would look like. Once you have organised people to push for a new world system, what is the policy framework that needs to be adopted? It is here that intellectuals must put their heart and soul into action.

## Read the Dossier

[The New Intellectual](https://thetricontinental.org/wp-content/uploads/2019/02/190213_Dossier_13_EN_Web2.pdf)

## Discussion



